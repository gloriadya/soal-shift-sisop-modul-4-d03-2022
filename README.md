# soal-shift-sisop-modul-4-D03-2022

Kelompok D 03 \
Anggota Kelompok 
|Nama                   |     NRP|
|-----------------------|----------------------|
|Naufal Fabian Wibowo    |    05111940000223|
|Gloria Dyah Pramesti    |    5025201033|
|Selfira Ayu Sheehan     |    5025201174|

Soal 1 Penjelasan
* 1a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13.
![1a](/uploads/bd6ad431b723c5a877fb4dc7e892b213/1a.png)
* Penjelasan Code:
* Pada function atbash_rot13(char *file) akan diperiksa terlebih dahulu apakah file berisi . atau .., jika merupakan salah satu dari pilihan tersebut, maka encode tidak dilakukan.
* Kemudian, dibuat variabel ext untuk menampung extension suatu file dan newFile untuk menampung nama file atau folder yang akan diencode(tanpa extension) yang masing-masing variabel bertipe string. Masing-masing variabel akan dilakukan memset yang bertujuan mengisi setiap char pada string supaya tidak menghasilkan karakter aneh pada output. Variabel lain yang dibuat yaitu n_ext yang akan memiliki panjang dari extension, n_file yang akan memiliki panjang dari nama file, dan flag yang digunakan sebagai penanda antara nama file dan extensionnya yang semua variabel tersebut diset memiliki nilai 0.
* Selanjutnya, akan dicari nama file atau folder beserta extensionnya jika ada yaitu dengan melakukan perulangan sebanyak strlen(file) atau panjang dari file. Pada perulangan, jika file[i] merupakan ., maka pada newFile akan ditambahkan . dan flag akan berubah nilainya menjadi 1 yang berarti sudah masuk pada fase extension. Namun, jika file[i] bukan merupakan ., maka akan diperiksa terlebih dahulu flag bernilai 0 atau 1, jika bernilai 0, maka file[i] akan ditambahkan pada newFile, jika sebaliknya, maka file[i] akan ditambahkan pada ext. n_file akan bertambah ketika newFile bertambah dan n_ext akan bertambah ketika ext bertambah.
* Lalu, newFile akan diencode dengan perulangan sebanyak n_file. Pada encode yang dilakukan, akan diperiksa apakah newFile[i] adalah uppercase atau lowercase. Jika merupakan uppercase, maka yang akan dilakukan adalah encode dengan atbash cipher yaitu menggunakan formula 'A' +'z' - newFile[i], tetapi jika merupakan lowercase, maka yang akan dilakukan adalah encode dengan rot13 yaitu penambahan 13 atau pengurangan 13. Pada encode dengan rot13, jika newFile merupakan karakter a hingga m maka akan dilakukan penambahan 13, tetapi jika newFile merupakan karakter n hingga z maka akan dilakukan pengurangan 13.
* Lalu, newFile dan ext akan digabungkan menggunakan strcat() dan hasilnya akan dicopy pada file menggunakan strcpy().

* Proses encode dilakukan apabila suatu directory memiliki path dengan awalan "Animeku_". Directory tersebut bisa sudah ada pada base root FUSE (langsung diterima oleh readdir dan getattr saat pembuatan FUSE) ataupun dibuat oleh user pada FUSE (menggunakan fungsi mkdir). Ketiga fungsi tersebut akan mendeteksi adanya directory dengan awalan "Animeku_" dan kemudian melakukan encode sesaat setelah user membuka suatu directory.
