#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static  const  char *dirpath = "/home/gloria/Documents";

void atbash_rot13(char *file){
    if (!strcmp(file, ".") || !strcmp(file, ".."))
        return;

    char ext[10], newFile[10000];

    memset(ext, 0, sizeof(ext));
    memset(newFile, 0, sizeof(newFile));

    int n_ext = 0, n_file = 0, flag = 0;

    //get file name and ext
    for (int i = 0; i < strlen(file); i++){
        if (file[i] == '.'){
            newFile[n_file++] = file[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
            ext[n_ext++] = file[i];
        else
            newFile[n_file++] = file[i];
    }

    //encode or decode file name
    for (int i = 0; i < n_file; i++){
        if (newFile[i] >= 65 && newFile[i] <= 90){
            newFile[i] = 'A' + 'Z' - newFile[i];
        }
        else if (newFile[i] >= 97 && newFile[i] <= 122){
            if (newFile[i] >= 97 && newFile[i] <= 109)
                newFile[i] += 13;
            else
                newFile[i] -= 13;
        }
    }

    //combine file name and ext
    strcat(newFile, ext);
    strcpy(file, newFile);
    printf("rot %s\n", file);
}
